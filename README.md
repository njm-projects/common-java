# COMMON - JAVA #

[![](https://jitpack.io/v/org.bitbucket.njm-projects/common-java.svg)](https://jitpack.io/#org.bitbucket.njm-projects/common-java)

This project is a collection of useful java (by that I mean kotlin) code that will be added to over time - all things that should be reused by my other projects.