package com.olhcim.common.time

import java.io.PrintStream
import kotlin.system.measureNanoTime

fun <T> printTime(prefix: String = "", postfix: String = "", printStream: PrintStream = System.out, block: () -> T): T {
    var result: T? = null
    val elapsed = measureNanoTime { result = block() }
    printStream.println("$prefix${elapsed / 1000_000.0}ms$postfix")
    return result!!
}

fun timeIt(n: Int = 1000, block: () -> Unit): Double {
    var total = 0L
    for (i in 0 until n)
        total += measureNanoTime(block)
    return total / n.toDouble()
}

fun measureTimeMillis(n: Int, block: () -> Unit): Triple<Long, Double, Long> {
    var total = 0L
    var min = Long.MAX_VALUE
    var max = Long.MIN_VALUE
    for(i in 0 until n) {
        val time = kotlin.system.measureTimeMillis(block)
        total += time
        if (time < min) min = time
        if (time > max) max = time
    }
    return Triple(min,total / n.toDouble(), max)
}

fun measureNanoTime(n: Int, block: () -> Unit): Triple<Long, Double, Long> {
    var total = 0L
    var min = Long.MAX_VALUE
    var max = Long.MIN_VALUE
    for(i in 0 until n) {
        val time = kotlin.system.measureNanoTime(block)
        total += time
        if (time < min) min = time
        if (time > max) max = time
    }
    return Triple(min,total / n.toDouble(), max)
}