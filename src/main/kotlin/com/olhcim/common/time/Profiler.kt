package com.olhcim.common.time

import com.olhcim.common.Level
import com.olhcim.common.Logger

/**
 * Created by Nathan on 2016/08/13.
 */
class Profiler(val logger: Logger? = null) {

    var ENABLED: Boolean = true

    private val valueProfilers = HashMap<String, ValueProfiler>()
    private val timerProfilers = HashMap<String, TimeProfiler>()

    fun profile(name: String, value: Double) {
        if(ENABLED)
            getValueProfiler(name).profile(value)
    }

    fun profile(name: String, block: () -> Unit) {
        if(ENABLED)
            getTimeProfiler(name).profile(block)
        else
            block()
    }

    fun printValue(name: String) {
        if(ENABLED)
            if(name in timerProfilers) loggerOrPrintln(timerProfilers[name].toString())
            else loggerOrPrintln("[$name] Does not exist!")
    }
    fun printTime(name: String) {
        if(ENABLED)
            if(name in timerProfilers) loggerOrPrintln(timerProfilers[name].toString())
            else loggerOrPrintln("[$name] Does not exist!")
    }

    private fun loggerOrPrintln(string: String) = logger?.log(Level.DEBUG, string) ?: println(string)

    private fun getValueProfiler(name: String): ValueProfiler {
        return if(name !in valueProfilers) {
            val profiler = ValueProfiler(name)
            valueProfilers[name] = profiler
            profiler
        } else valueProfilers[name]!!
    }

    protected fun getTimeProfiler(name: String): TimeProfiler {
        return if(name !in timerProfilers) {
            val profiler = TimeProfiler(name)
            timerProfilers[name] = profiler
            profiler
        } else timerProfilers[name]!!
    }

    companion object {
        val INSTANCE = Profiler()
    }
}
