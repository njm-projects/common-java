package com.olhcim.common.time

import java.lang.Math.sqrt
import kotlin.system.measureNanoTime


class ValueProfiler(val name: String) {
    var min = Double.MAX_VALUE
        private set
    var max = Double.MIN_VALUE
        private set
    var ave: Double = 0.0
        private set
    var total: Double = 0.0
        private set
    var count: Long = 0
        private set

    var movingVar: Double = 0.0
    var movingAve: Double = 0.0

    fun profile(value: Double) {
        if (value < min)
            min = value
        if (value > max)
            max = value

        movingVar = (movingVar + count*(movingVar - movingAve*value))/(count+2)
        movingAve -= (movingAve - value) / (count + 1)

        total += value
        count += 1

        ave = total / count
    }

    override fun toString(): String {
        return "[$name] COUNT: $count AVE: TOTAL: $total $ave MIN: $min MAX: $max  MOVING VAR: $movingVar MOVING AVE: $movingAve"
    }
}

class TimeProfiler(name: String) {
    private val valueProfiler = ValueProfiler(name)

    val min get() = valueProfiler.min
    val max get() = valueProfiler.max
    val ave get() = valueProfiler.ave
    val total get() = valueProfiler.total
    val count get() = valueProfiler.count
    val movingAve get() = valueProfiler.movingAve
    val movingVar get() = valueProfiler.movingVar

    fun profile(obj: () -> Unit) = valueProfiler.profile(measureNanoTime(obj).toDouble())

    override fun toString() = valueProfiler.toString()
}