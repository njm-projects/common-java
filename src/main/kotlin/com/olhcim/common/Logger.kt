
/*
 * Created on 2017/5/21 by Nathan Michlo.
 */

package com.olhcim.common

import com.olhcim.common.exception.stackTraceString
import com.olhcim.common.stream.printlnLinesPrefixed
import java.io.PrintStream
import kotlin.system.measureNanoTime

enum class Level(val levelName: String) {
    VERBOSE("VRBS"),
    DEBUG("DBUG"),
    STANDARD("NORM"),
    WARN("WARN"),
    ERROR("ERRR");
}

class Logger(
        private val logLevel: Level? = Level.STANDARD,
        private val printStream: PrintStream,
        private val linePrefix: String? = null
) {

    private val caller: String
        get() {
            val stElements = Thread.currentThread().stackTrace
            for (i in 1 until stElements.size) {
                val ste = stElements[i]
                if (ste.className != this::class.java.name && ste.className.indexOf("java.lang.Thread") != 0)
                    return ste.className
            }
            return "null"
        }

    fun canLog(level: Level) = (logLevel != null) && (level.ordinal >= logLevel.ordinal)

    private fun internalLog(message: String, level: Level, caller: String) {
        val split = message.split('\n')
        if (linePrefix == null) {
            split.joinTo(printStream, separator = "\n\t| ", prefix = "[${level.levelName}] [$caller] - ", postfix = "\n")
        } else {
            split.joinTo(printStream, separator = "\n$linePrefix\t| ", prefix = "$linePrefix[${level.levelName}] [$caller] - ", postfix = "\n")
        }
    }

    fun log(level: Level, message: String) {
        if(canLog(level))
            internalLog(message, level, caller)
    }

    fun log(level: Level, message: Any) {
        if(canLog(level))
            internalLog(message.toString(), level, caller)
    }

    inline fun log(level: Level, message: () -> String) {
        if(canLog(level))
            log(level, message())
    }

    inline fun <T> logTime(level: Level, name: String, duelLog: Boolean = false, block: () -> T): T {
        var result: T? = null
        if(duelLog)
            log(level, name)
        val elapsed = measureNanoTime { result = block() }
        log(level) { "$name: ${elapsed / 1000_000.0}ms" }
        return result!!
    }

    inline fun exceptLog(string: String? = null, block: () -> Unit) {
        try {
            block.invoke()
        } catch (e: Exception) {
            exceptLog(string, e)
        }
    }

    fun exceptLog(message: String) {
        exceptLog(null, IllegalStateException(message))
    }

    fun exceptLog(string: String?, e: Throwable) {
        if(string != null)
            log(Level.ERROR,"[${string.toUpperCase()}]")
        log(Level.ERROR, e.stackTraceString)
    }
}