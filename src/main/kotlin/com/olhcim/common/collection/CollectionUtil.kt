package com.olhcim.common.collection



fun DoubleArray.argMax(): Int {
    var i = 0
    var max = Double.MIN_VALUE
    var argmax = 0
    while(i < this.size) {
        if(this[i] > max) {
            max = this[i]
            argmax = i
        }
        i += 1
    }
    return argmax
}

fun IntArray.argMax(): Int {
    var i = 0
    var max = Int.MIN_VALUE
    var argmax = 0
    while(i < this.size) {
        if(this[i] > max) {
            max = this[i]
            argmax = i
        }
        i += 1
    }
    return argmax
}

fun DoubleArray.argMin(): Int {
    var i = 0
    var min = Double.MAX_VALUE
    var argmin = 0
    while(i < this.size) {
        if(this[i] < min) {
            min = this[i]
            argmin = i
        }
        i += 1
    }
    return argmin
}

fun IntArray.argMin(): Int {
    var i = 0
    var min = Int.MAX_VALUE
    var argmin = 0
    while(i < this.size) {
        if(this[i] < min) {
            min = this[i]
            argmin = i
        }
        i += 1
    }
    return argmin
}

fun DoubleArray.product(): Double {
    var product = 1.0
    for (element in this) product *= element
    return product
}

fun IntArray.product(): Int {
    var product = 1
    for (element in this) product *= element
    return product
}