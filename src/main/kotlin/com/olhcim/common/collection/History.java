package com.olhcim.common.collection;

import java.lang.reflect.Array;

/**
 * Created by Nathan on 2016/09/01.
 */
public class History<T> {

    public static final int DEFAULT_HISTORY_SIZE = 5;

    private final int maxSize;
    private int front;
    private int back;
    private int size;

    private final T[] queue;

    public History(Class<T> klass) {
        this(klass, DEFAULT_HISTORY_SIZE);
    }

    public History(Class<T> klass, int maxSize) {
        this.maxSize = maxSize;
        this.front = 0;
        this.back = 0;
        this.size = 0;
        queue =  (T[]) Array.newInstance(klass, maxSize);
    }

    public void clear() {
        this.front = 0;
        this.back = 0;
        this.size = 0;
    }

    public void clear(int amount) {
        size -= amount;
        front += amount;
        front %= maxSize;
        if(size <= 0)
            clear();
    }

    public int size() {
        return size;
    }

    public void push_back(T t) {
        queue[back] = t;
        back += 1;
        back %= maxSize;
        if(size < maxSize) {
            size += 1;
        } else {
            front += 1;
            front %= maxSize;
        }
    }

    public void push_front(T t) {
        front += maxSize - 1;
        front %= maxSize;
        if(size < maxSize) {
            size += 1;
        } else {
            back += maxSize - 1;
            back %= maxSize;
        }
        queue[front] = t;
    }

    public T top() {
        return get(0);
    }

    public void push(T t) {
        push_front(t);
    }

    public T get(int i) {
        if(i < 0 || i > size-1)
            throw new IndexOutOfBoundsException();
        int index = (front + i) % maxSize;
        return queue[index];
    }

    public T safeGet(int i) {
        if(i < 0)
            i = 0;
        if(i > maxSize-1)
            i = size-1;
        int index = (front + i) % maxSize;
        return queue[index];
    }
}
