package com.olhcim.common.collection.circular

import java.util.AbstractList
import java.util.RandomAccess

abstract class AbstractFixedCircularArray<T> : AbstractList<T>(), RandomAccess {

    final override var size: Int = 0
        protected set
    final protected var first: Int = 0

    abstract val capacity: Int

    /**
     * Shared Methods
     */

    final protected fun wrapIndex(index: Int): Int {
        var temp = index % capacity
        if (temp < 0)
            temp += capacity
        return temp
    }

    final inline fun peekFront() = this[0]
    final inline fun peekBack() = this[size - 1]

    final inline fun pop() = popFront()
    final inline fun push(value: T) = pushBack(value)
    final override fun add(element: T): Boolean { pushBack(element); return true }

    final fun pushConstrained(value: T) {
        if(size >= capacity)
            pop()
        push(value)
    }

    final inline fun first() = peekFront()
    final inline fun last() = peekBack()
    final inline fun peek() = peekFront()

    /**
     * Abstract Methods
     */

    abstract override operator fun get(index: Int): T
    abstract override operator fun set(index: Int, element: T): T

    abstract fun pushFront(value: T)
    abstract fun popFront() : T
    abstract fun pushBack(value: T)
    abstract fun popBack() : T

    abstract override fun clear()

}