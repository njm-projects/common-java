package com.olhcim.common.collection.circular

open class FixedCircularIntArray(capacity: Int, generator: ((Int) -> Int)? = null) : AbstractFixedCircularArray<Int>() {

    constructor() : this(10)

    override val capacity get() = array.size //To change initializer of created properties use File | Settings | File Templates.

    protected var array: IntArray

    init {
        if(generator != null) {
            this.size = capacity
            this.array = IntArray(capacity, generator)
        } else {
            this.size = 0
            this.array = IntArray(capacity)
        }
    }

    override operator fun get(index: Int): Int {
        if (index < 0 || index >= size)
            throw IndexOutOfBoundsException("$index")
        return array[wrapIndex(first + index)]
    }

    override operator fun set(index: Int, element: Int): Int {
        if (index < 0 || index >= size)
            throw IndexOutOfBoundsException()
        val i = wrapIndex(first + index)
        array[i] = element
        return array[i]
    }

    override fun pushFront(value: Int) {
        if(size >= capacity)
            popBack()
        first = wrapIndex(first - 1)
        array[first] = value
        size += 1
    }

    override fun popFront() : Int {
        if(size <= 0)
            throw IllegalStateException("Cannot pop element from empty list!")
        val value = array[first]
        first = wrapIndex(first + 1)
        size -= 1
        return value
    }

    override fun pushBack(value: Int) {
        if(size >= capacity)
            popFront()
        array[wrapIndex(first + size)] = value
        size += 1
    }

    override fun popBack() : Int {
        if(size <= 0)
            throw IllegalStateException("Cannot pop element from empty list!")
        val i = wrapIndex(first + size - 1)
        val value = array[i]
        size -= 1
        return value
    }

    override fun clear() {
        this.size = 0
        this.first = 0
    }
}