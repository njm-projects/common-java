package com.olhcim.common.collection.circular

open class CircularArray<T> (capacity: Int, generator: ((Int) -> T)? = null) : FixedCircularArray<T>(capacity, generator), IResizableArray<T> {

    override fun pushFront(value: T) {
        if(size >= capacity)
            grow()
        first = wrapIndex(first - 1)
        array[first] = value
        size += 1
    }

    override fun pushBack(value: T) {
        if(size >= capacity)
            grow()
        array[wrapIndex(first + size)] = value
        size += 1
    }

    override fun setCapacity(newCapacity: Int) {
        if(newCapacity < size)
            throw RuntimeException("Capacity cannot be smaller than the size!")
        if(newCapacity != capacity) {
            val newArray = Array<Any?>(newCapacity) { null }
            for(i in 0 until size)
                newArray[i] = get(i)
            array = newArray
            first = 0
        }
    }
}