package com.olhcim.common.collection.circular

interface IResizableArray<T> : Collection<T>, RandomAccess {
    val capacity: Int
    override val size: Int

    fun setCapacity(newCapacity: Int)
    fun grow() = setCapacity(Math.ceil(capacity * 1.618).toInt())
    fun shrink() = setCapacity(size)
}