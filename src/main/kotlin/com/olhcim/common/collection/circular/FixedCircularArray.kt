package com.olhcim.common.collection.circular

open class FixedCircularArray<T> (capacity: Int, generator: ((Int) -> T)? = null) : AbstractFixedCircularArray<T>() {

    constructor() : this(10)

    override val capacity get() = array.size

    protected var array: Array<Any?>

    init {
        if(generator != null) {
            this.size = capacity
            this.array = Array(capacity, generator)
        } else {
            this.size = 0
            this.array = Array(capacity) { null }
        }
    }

    override operator fun get(index: Int): T {
        if (index !in 0 until size)
            throw IndexOutOfBoundsException("$index")
        return array[wrapIndex(first + index)]!! as T
    }

    override operator fun set(index: Int, element: T): T {
        if (index !in 0 until size)
            throw IndexOutOfBoundsException()
        val i = wrapIndex(first + index)
        array[i] = element
        return array[i]!! as T
    }

    override fun pushFront(value: T) {
        if(size >= capacity)
            popBack()
        first = wrapIndex(first - 1)
        array[first] = value
        size += 1
    }

    override fun popFront() : T {
        if(size <= 0)
            throw IllegalStateException("Cannot pop element from empty list!")
        val value = array[first] as T
        array[first] = null
        first = wrapIndex(first + 1)
        size -= 1
        return value
    }

    override fun pushBack(value: T) {
        if(size >= capacity)
            popFront()
        array[wrapIndex(first + size)] = value
        size += 1
    }

    override fun popBack() : T {
        if(size <= 0)
            throw IllegalStateException("Cannot pop element from empty list!")
        val i = wrapIndex(first + size - 1)
        val value = array[i] as T
        array[i] = null
        size -= 1
        return value
    }

    override fun clear() {
        this.size = 0
        while(size > 0) {
            array[first] = null
            first = wrapIndex(first + 1)
            size -= 1
        }
        this.first = 0
    }
}