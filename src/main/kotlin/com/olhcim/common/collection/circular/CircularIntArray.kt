package com.olhcim.common.collection.circular

class CircularIntArray(capacity: Int, generator: ((Int) -> Int)? = null) : FixedCircularIntArray(capacity, generator), IResizableArray<Int> {

    override fun pushFront(value: Int) {
        if(size >= capacity)
            grow()
        first = wrapIndex(first - 1)
        array[first] = value
        size += 1
    }

    override fun pushBack(value: Int) {
        if(size >= capacity)
            grow()
        array[wrapIndex(first + size)] = value
        size += 1
    }

    override fun setCapacity(newCapacity: Int) {
        if(newCapacity < size)
            throw RuntimeException("Capacity cannot be smaller than the size!")
        if(newCapacity != capacity) {
            val newArray = IntArray(newCapacity)
            for(i in 0 until size)
                newArray[i] = get(i)
            array = newArray
            first = 0
        }
    }
}