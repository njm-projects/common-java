package com.olhcim.common.collection

class SubListIndices<T>(val list: List<T>, val indices: List<Int>) : AbstractList<T>(), RandomAccess {
    override val size get() = indices.size
    override fun get(index: Int) = list[indices[index]]
}

class SubListRange<T>(val list: List<T>, val range: IntRange) : AbstractList<T>(), RandomAccess {
    override val size get() = range.start - range.endInclusive + 1
    override fun get(index: Int) = list[index + range.start]
}