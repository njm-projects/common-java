
package com.olhcim.common.exception

import java.io.PrintWriter
import java.io.StringWriter

val Throwable.stackTraceString: String
    get() {
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        this.printStackTrace(pw)
        return sw.toString()
    }