package com.olhcim.common

/**
 * Created by Nathan on 2016/09/04.
 */
interface Copyable {
    fun copy(): Copyable
}
