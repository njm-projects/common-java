package com.olhcim.common.thread


class BackgroundThread(private val block: () -> Unit) {

    var isRunning = false
        private set
    var isFinished = false
        private set
    var hasError = false
        private set

    val active get() = isRunning || isFinished

    fun start() : BackgroundThread {
        if(isRunning)
            throw IllegalStateException("Already Running!")
        if(isFinished)
            throw IllegalStateException("Already Finished!")
        isRunning = true
        val thread = Thread {
            try {
                block()
            } catch (e: Exception) {
                hasError = true
            }
            isFinished = true
            isRunning = false
        }
        thread.start()
        return this
    }
}