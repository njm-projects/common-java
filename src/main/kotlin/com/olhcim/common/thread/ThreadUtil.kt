package com.olhcim.common.thread

fun interrupted() = Thread.currentThread().isInterrupted