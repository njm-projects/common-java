


package com.olhcim.common.stream

import java.io.FileInputStream
import java.io.PrintStream

fun PrintStream.printlnLinesPrefixed(any: Any, prefix: String = "") {
    val lines = any.toString().split("\n")
    for (line in lines)
        this.println("$prefix$line")
}

fun printBreak(str: String, size: Int = 80) {
    val len = str.length
    if(len > size - 6)
        throw RuntimeException("String too large")
    val bar = "/* ${(0 until size - 6).joinToString("") { "=" }} */"
    println()
    println(bar)
    println("/* $str${(0 until size - str.length - 6).joinToString("") { " " }} */")
    println(bar)
    println()
}

fun FileInputStream.readByte(): Int = read()
fun FileInputStream.readUnsignedByte(): Int = (read() and 0xFF)
fun FileInputStream.readShort(): Int = ((read() shl 8) or (read()))
fun FileInputStream.readInteger(): Int = (read() shl 24) or (read() shl 16) or (read() shl 8) or (read())
