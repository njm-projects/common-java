package com.olhcim.common.data

import com.olhcim.common.collection.product
import com.olhcim.common.stream.readInteger
import com.olhcim.common.stream.readShort
import com.olhcim.common.stream.readUnsignedByte
import java.io.File
import java.io.FileInputStream


enum class IdxDataType(val code: Int, val bytes: Int) {
    UNSIGNED_BYTE   (0x08, 1),
    BYTE            (0x09, 1),
    SHORT           (0x0B, 2),
    INT             (0x0C, 4),
    FLOAT           (0x0D, 4),
    DOUBLE          (0x0E, 8),
    UNKNOWN         (-1,  -1);

    companion object {
        fun fromCode(code: Int) = when (code) {
            UNSIGNED_BYTE.code -> UNSIGNED_BYTE
            BYTE.code -> BYTE
            SHORT.code -> SHORT
            INT.code -> INT
            FLOAT.code -> FLOAT
            DOUBLE.code -> DOUBLE
            else -> UNKNOWN
        }
    }
}

class IdxObject(fileName: String) {

    val type: IdxDataType
    val dimNum: Int
    val dimSizes: IntArray
    val dataArrayLength: Int
    val byteArrayLength: Int
    val bytes: ByteArray

    init {
        var fis: FileInputStream? = null

        try {
            fis = FileInputStream(File(fileName))
            val zero = fis.readShort()
            type = IdxDataType.fromCode(fis.readUnsignedByte())
            dimNum = fis.readUnsignedByte()
            dimSizes = IntArray(dimNum) { fis.readInteger() }
            dataArrayLength = dimSizes.product()
            byteArrayLength = dataArrayLength * type.bytes
            bytes = fis.readBytes()
        } catch (e: Exception) {
            fis?.close()
            throw e
        } finally {
            fis?.close()
        }

        check(bytes.size == byteArrayLength && byteArrayLength % type.bytes == 0) { "Invalid sized Idx file" }
    }
}