package com.olhcim.common.math.expressions



/* ========================================================================== */
/* Interface                                                                  */
/* ========================================================================== */

interface Exp<T> : RandomAccess {
    val size: Int
    operator fun get(i: Int): T
}



/* ========================================================================== */
/* Basic Expression Operators - Left for rapid prototyping... Originally used */
/* classes, this is 2x faster but still slow, use hard coded inline functions */
/* ========================================================================== */

infix fun <T> Exp<T>.to(mutator: (T) -> T) : ((i: Int) -> T) = { mutator(this[it]) }

inline operator fun Exp<Double>.unaryPlus() = this
inline operator fun Exp<Double>.unaryMinus() : ((i: Int) -> Double) = { -this[it] }

inline operator fun Exp<Double>.plus (b: Exp<Double>) : ((i: Int) -> Double) = { this[it] + b[it] }
inline operator fun Exp<Double>.minus(b: Exp<Double>) : ((i: Int) -> Double) = { this[it] - b[it] }
inline operator fun Exp<Double>.times(b: Exp<Double>) : ((i: Int) -> Double) = { this[it] * b[it] }
inline operator fun Exp<Double>.div  (b: Exp<Double>) : ((i: Int) -> Double) = { this[it] / b[it] }
inline operator fun Exp<Double>.rem  (b: Exp<Double>) : ((i: Int) -> Double) = { this[it] % b[it] }

inline operator fun Exp<Double>.plus (crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this[it] + operator(it) }
inline operator fun Exp<Double>.minus(crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this[it] - operator(it) }
inline operator fun Exp<Double>.times(crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this[it] * operator(it) }
inline operator fun Exp<Double>.div  (crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this[it] / operator(it) }
inline operator fun Exp<Double>.rem  (crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this[it] % operator(it) }

inline operator fun Exp<Double>.plus (value: Double) : ((i: Int) -> Double) = { this[it] + value }
inline operator fun Exp<Double>.minus(value: Double) : ((i: Int) -> Double) = { this[it] - value }
inline operator fun Exp<Double>.times(value: Double) : ((i: Int) -> Double) = { this[it] * value }
inline operator fun Exp<Double>.div  (value: Double) : ((i: Int) -> Double) = { this[it] / value }
inline operator fun Exp<Double>.rem  (value: Double) : ((i: Int) -> Double) = { this[it] % value }

inline operator fun ((i: Int) -> Double).plus (arr: Exp<Double>) : ((i: Int) -> Double) = { this(it) + arr[it] }
inline operator fun ((i: Int) -> Double).minus(arr: Exp<Double>) : ((i: Int) -> Double) = { this(it) - arr[it] }
inline operator fun ((i: Int) -> Double).times(arr: Exp<Double>) : ((i: Int) -> Double) = { this(it) * arr[it] }
inline operator fun ((i: Int) -> Double).div  (arr: Exp<Double>) : ((i: Int) -> Double) = { this(it) / arr[it] }
inline operator fun ((i: Int) -> Double).rem  (arr: Exp<Double>) : ((i: Int) -> Double) = { this(it) % arr[it] }

inline operator fun ((i: Int) -> Double).plus (crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this(it) + operator(it) }
inline operator fun ((i: Int) -> Double).minus(crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this(it) - operator(it) }
inline operator fun ((i: Int) -> Double).times(crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this(it) * operator(it) }
inline operator fun ((i: Int) -> Double).div  (crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this(it) / operator(it) }
inline operator fun ((i: Int) -> Double).rem  (crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this(it) % operator(it) }

inline operator fun ((i: Int) -> Double).plus (value: Double) : ((i: Int) -> Double) = { this(it) + value }
inline operator fun ((i: Int) -> Double).minus(value: Double) : ((i: Int) -> Double) = { this(it) - value }
inline operator fun ((i: Int) -> Double).times(value: Double) : ((i: Int) -> Double) = { this(it) * value }
inline operator fun ((i: Int) -> Double).div  (value: Double) : ((i: Int) -> Double) = { this(it) / value }
inline operator fun ((i: Int) -> Double).rem  (value: Double) : ((i: Int) -> Double) = { this(it) % value }

inline operator fun Double.plus (arr: Exp<Double>) : ((i: Int) -> Double) = { this + arr[it] }
inline operator fun Double.minus(arr: Exp<Double>) : ((i: Int) -> Double) = { this - arr[it] }
inline operator fun Double.times(arr: Exp<Double>) : ((i: Int) -> Double) = { this * arr[it] }
inline operator fun Double.div  (arr: Exp<Double>) : ((i: Int) -> Double) = { this / arr[it] }
inline operator fun Double.rem  (arr: Exp<Double>) : ((i: Int) -> Double) = { this % arr[it] }

inline operator fun Double.plus (crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this + operator(it) }
inline operator fun Double.minus(crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this - operator(it) }
inline operator fun Double.times(crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this * operator(it) }
inline operator fun Double.div  (crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this / operator(it) }
inline operator fun Double.rem  (crossinline operator: (i: Int) -> Double) : ((i: Int) -> Double) = { this % operator(it) }



/* ========================================================================== */
/* Expression Operators                                                       */
/* ========================================================================== */

class ExpMatmulOuter(private val A: Exp<Double>, private val B: Exp<Double>) : Exp<Double>, IIndices2D {
    override val size = A.size * B.size
    override val cols = B.size
    override fun get(i: Int) = A[indexToRow(i)] * B[indexToCol(i)]
}

infix fun Exp<Double>.vecmulOuter(B: Exp<Double>) = ExpMatmulOuter(this, B)

/* ========================================================================== */
/* Expression Utils                                                           */
/* ========================================================================== */

fun Exp<Double>.max(): Double { var max = Double.MIN_VALUE; for(i in 0 until size) { val v = this[i]; if(v > max) max = v; }; return max }
fun Exp<Double>.min(): Double { var min = Double.MAX_VALUE; for(i in 0 until size) { val v = this[i]; if(v < min) min = v; }; return min }
fun Exp<Double>.minmax(): Pair<Double, Double> { var min = Double.MAX_VALUE; var max = Double.MIN_VALUE; for(i in 0 until size) { val v = this[i]; if(v < min) min = v; if(this[i] > max) max = this[i]; }; return Pair(min, max) }

fun Exp<Double>.argmax(): Int { var max = Double.MIN_VALUE; var index = 0; for(i in 0 until size) if(this[i] > max) { max = this[i]; index = i }; return index }
fun Exp<Double>.argmin(): Int { var min = Double.MAX_VALUE; var index = 0; for(i in 0 until size) if(this[i] < min) { min = this[i]; index = i }; return index }



























//this version acheived very good performance before the rewrite because of the non-private mutator values

///* ========================================================================== */
///* Expressions                                                                */
///* ========================================================================== */
//
//
//interface Exp<T> : RandomAccess {
//    val size: Int
//    operator fun get(i: Int): T
//}
//
//abstract class ExpOperator<out E1: Exp<Double>, out E2: Exp<Double>>(
//        protected val a: E1,
//        protected val b: E2
//) : Exp<Double> { init { check(a.size == b.size) { "Expression sizes differ! A:${a.size} B:${b.size}" } } }
//
//class ExpPlus<out E1: Exp<Double>, out E2: Exp<Double>>(a: E1, b: E2) : ExpOperator<E1, E2>(a, b)  { override fun get(i: Int) = a[i] + b[i]; override val size get() = b.size }
//class ExpMinus<out E1: Exp<Double>, out E2: Exp<Double>>(a: E1, b: E2) : ExpOperator<E1, E2>(a, b) { override fun get(i: Int) = a[i] - b[i]; override val size get() = b.size }
//class ExpTimes<out E1: Exp<Double>, out E2: Exp<Double>>(a: E1, b: E2) : ExpOperator<E1, E2>(a, b) { override fun get(i: Int) = a[i] * b[i]; override val size get() = b.size }
//class ExpDiv<out E1: Exp<Double>, out E2: Exp<Double>>(a: E1, b: E2) : ExpOperator<E1, E2>(a, b)   { override fun get(i: Int) = a[i] / b[i]; override val size get() = b.size }
//class ExpRem<out E1: Exp<Double>, out E2: Exp<Double>>(a: E1, b: E2) : ExpOperator<E1, E2>(a, b)   { override fun get(i: Int) = a[i] % b[i]; override val size get() = b.size }
//
//class ExpNegate(val exp: Exp<Double>) : Exp<Double> { override fun get(i: Int) = -exp[i]; override val size get() = exp.size }
//
//// being private reduces performance by 400%
//class ExpMutator<T>(val exp: Exp<T>, val mutator: (a: T) -> T) : Exp<T> { override fun get(i: Int) = mutator.invoke(exp[i]); override val size get() = exp.size }
//
//
///* ========================================================================== */
///* Expressions Operators                                                      */
///* ========================================================================== */
//
//operator fun Exp<Double>.unaryPlus() = this
//operator fun Exp<Double>.unaryMinus() = ExpNegate(this)
//
//operator fun Exp<Double>.plus(const: Double): Exp<Double>  = ExpPlus(this, ConstArray1D(this.size, const))
//operator fun Exp<Double>.minus(const: Double): Exp<Double> = ExpMinus(this, ConstArray1D(this.size, const))
//operator fun Exp<Double>.times(const: Double): Exp<Double> = ExpTimes(this, ConstArray1D(this.size, const))
//operator fun Exp<Double>.div(const: Double): Exp<Double>   = ExpDiv(this, ConstArray1D(this.size, const))
//operator fun Exp<Double>.rem(const: Double): Exp<Double>   = ExpRem(this, ConstArray1D(this.size, const))
//
//operator fun Exp<Double>.plus(exp: Exp<Double>): Exp<Double>  = ExpPlus(this, exp)
//operator fun Exp<Double>.minus(exp: Exp<Double>): Exp<Double> = ExpMinus(this, exp)
//operator fun Exp<Double>.times(exp: Exp<Double>): Exp<Double> = ExpTimes(this, exp)
//operator fun Exp<Double>.div(exp: Exp<Double>): Exp<Double>   = ExpDiv(this, exp)
//operator fun Exp<Double>.rem(exp: Exp<Double>): Exp<Double>   = ExpRem(this, exp)
//
//operator fun Double.plus(exp: Exp<Double>): Exp<Double>  = if(this != 0.0) ExpTimes(ConstArray1D(exp.size, this), exp) else exp
//operator fun Double.minus(exp: Exp<Double>): Exp<Double> = if(this != 0.0) ExpTimes(ConstArray1D(exp.size, this), exp) else -exp
//operator fun Double.times(exp: Exp<Double>): Exp<Double> = if(this != 0.0) ExpTimes(ConstArray1D(exp.size, this), exp) else ConstArray1D(exp.size, 0.0)
//operator fun Double.div(exp: Exp<Double>): Exp<Double>   = ExpDiv(ConstArray1D(exp.size, this), exp)
//operator fun Double.rem(exp: Exp<Double>): Exp<Double>   = ExpRem(ConstArray1D(exp.size, this), exp)
//
//operator fun IArray1D<Double>.plusAssign(const: Double)  { for(i in 0 until size) this[i] += const }
//operator fun IArray1D<Double>.minusAssign(const: Double) { for(i in 0 until size) this[i] -= const }
//operator fun IArray1D<Double>.timesAssign(const: Double) { for(i in 0 until size) this[i] *= const }
//operator fun IArray1D<Double>.divAssign(const: Double)   { for(i in 0 until size) this[i] /= const }
//operator fun IArray1D<Double>.remAssign(const: Double)   { for(i in 0 until size) this[i] %= const }
//
//operator fun IArray1D<Double>.plusAssign(exp: Exp<Double>)  { for(i in 0 until size) this[i] += exp[i] }
//operator fun IArray1D<Double>.minusAssign(exp: Exp<Double>) { for(i in 0 until size) this[i] -= exp[i] }
//operator fun IArray1D<Double>.timesAssign(exp: Exp<Double>) { for(i in 0 until size) this[i] *= exp[i] }
//operator fun IArray1D<Double>.divAssign(exp: Exp<Double>)   { for(i in 0 until size) this[i] /= exp[i] }
//operator fun IArray1D<Double>.remAssign(exp: Exp<Double>)   { for(i in 0 until size) this[i] %= exp[i] }
//
//infix fun <T> IArray1D<T>.set(exp: Exp<T>)    { for(i in 0 until size) this[i] = exp[i] }
//
//infix fun <T> Exp<T>.apply(mutator: (T)->T) = ExpMutator(this, mutator)
//
//
//
//
//
//
///* ========================================================================== */
///* Expression Utils                                                           */
///* ========================================================================== */
//
//infix fun Exp<Double>.vecmulOuter(B: Exp<Double>) = ExpMatmulOuter(this, B)
//
//class ExpMatmulOuter(private val A: Exp<Double>, private val B: Exp<Double>) : Exp<Double>, IIndices2D {
//    override val size = A.size * B.size
//    override val cols = B.size
//    override fun get(i: Int) = A[indexToRow(i)] * B[indexToCol(i)]
//}
//
//fun Exp<Double>.max(): Double { var max = Double.MIN_VALUE; for(i in 0 until size) if(this[i] > max) max = this[i]; return max }
//fun Exp<Double>.min(): Double { var min = Double.MAX_VALUE; for(i in 0 until size) if(this[i] < min) min = this[i]; return min }
//
//fun Exp<Double>.argmax(): Int { var max = Double.MIN_VALUE; var index = 0; for(i in 0 until size) if(this[i] > max) { max = this[i]; index = i }; return index }
//fun Exp<Double>.argmin(): Int { var min = Double.MAX_VALUE; var index = 0; for(i in 0 until size) if(this[i] < min) { min = this[i]; index = i }; return index }
//
//fun Exp<Double>.merge() = Array1DDouble(DoubleArray(size){ this@merge[it] })
//
///* ========================================================================== */
///* Array Interfaces                                                           */
///* ========================================================================== */
//
//interface IArray1D<T> : Exp<T> {
//    operator fun set(i: Int, value: T)
//}
//
//interface IIndices2D {
//    val cols: Int
//    fun rowColToIndex(row: Int, col: Int): Int = (row* cols)+col
//    fun indexToCol(i: Int): Int = i % cols
//    fun indexToRow(i: Int): Int = i / cols
//}
//
//interface IArray2D<T> : IArray1D<T>, IIndices2D {
//    val rows: Int
//    override val cols: Int
//    override val size: Int get() = rows * cols
//    operator fun set(row: Int, col: Int, value: T)
//    operator fun get(row: Int, col: Int): T
//}
//
///* ========================================================================== */
///* Const Array Implementations                                                */
///* ========================================================================== */
//
//class ConstArray1D<T>(override val size: Int, val const: T) : IArray1D<T> {
//    inline override fun get(i: Int) = const
//    override fun set(i: Int, value: T) { throw UnsupportedOperationException("Cannot set a const array") }
//}
//
//class ConstArray2D<T>(override val rows: Int, override val cols: Int, private val const: T) : IArray2D<T> {
//    override val size = rows*cols
//    override fun get(i: Int) = const
//    override fun set(i: Int, value: T) { throw UnsupportedOperationException("Cannot set a const array") }
//    override fun get(row: Int, col: Int) = const
//    override fun set(row: Int, col: Int, value: T) { throw UnsupportedOperationException("Cannot set a const array") }
//}
//
///* ========================================================================== */
///* Array Implementations                                                      */
///* ========================================================================== */
//
//open class Array1DDouble(val array: DoubleArray) : IArray1D<Double> {
//    override val size = array.size
//    constructor(size: Int, generator: ((i: Int) -> Double)? = null) : this(DoubleArray(size)) {
//        if(generator != null) for(i in 0 until size) array[i] = generator.invoke(i)
//    }
//    override fun get(i: Int) = array[i]
//    override fun set(i: Int, value: Double) { array[i] = value }
//}
//
//open class Array1DInt(val array: IntArray) : IArray1D<Int> {
//    override val size = array.size
//    constructor(size: Int, generator: ((i: Int) -> Int)? = null) : this(IntArray(size)) {
//        if(generator != null) for(i in 0 until size) array[i] = generator.invoke(i)
//    }
//    override fun get(i: Int) = array[i]
//    override fun set(i: Int, value: Int) { array[i] = value }
//}
//
//open class Array2DDouble(
//        override val rows: Int,
//        override val cols: Int,
//        val array: DoubleArray
//) : IArray2D<Double> {
//    init { check(size == array.size) }
//    constructor(rows: Int, cols: Int, generator: ((row: Int, col: Int) -> Double)? = null) : this(rows, cols, DoubleArray(rows*cols)) {
//        if(generator != null) for(i in 0 until size) array[i] = generator.invoke(indexToRow(i), indexToCol(i))
//    }
//    override fun get(i: Int) = array[i]
//    override fun set(i: Int, value: Double) { array[i] = value }
//    override fun get(row: Int, col: Int) = array[rowColToIndex(row, col)]
//    override fun set(row: Int, col: Int, value: Double) { array[rowColToIndex(row, col)] = value }
//}
//
//open class Array2DInt(
//        override val rows: Int,
//        override val cols: Int,
//        val array: IntArray
//) : IArray2D<Int> {
//    init { check(size == array.size) }
//    constructor(rows: Int, cols: Int, generator: ((row: Int, col: Int) -> Int)? = null) : this(rows, cols, IntArray(rows*cols)) {
//        if(generator != null) for(i in 0 until size) array[i] = generator.invoke(indexToRow(i), indexToCol(i))
//    }
//    override fun get(i: Int) = array[i]
//    override fun set(i: Int, value: Int) { array[i] = value }
//    override fun get(row: Int, col: Int) = array[rowColToIndex(row, col)]
//    override fun set(row: Int, col: Int, value: Int) { array[rowColToIndex(row, col)] = value }
//}
//
//
///* ========================================================================== */
///* Matrices                                                                   */
///* ========================================================================== */
//
//
//
//interface IMatrix : IArray2D<Double> {
//
//}
//
//class Matrix(
//        rows: Int,
//        cols: Int,
//        generator: ((row: Int, col: Int) -> Double)? = null
//) : IMatrix, Array2DDouble(
//        rows = rows,
//        cols = cols,
//        generator = generator
//) {
//
//    val isColVector get() = (cols == 1)
//    val isRowVector get() = (rows == 1)
//    val isVector get() = isColVector || isRowVector
//
//    /* ========================================================================== */
//    /* Constructors                                                               */
//    /* ========================================================================== */
//
//    constructor(data: DoubleArray) : this(data.size, 1, data)
//
//    constructor(rows: Int) : this(rows, 1)
//
//    constructor(rows: Int, generator: (i: Int) -> Double) : this(rows, 1) {
//        for(i in 0 until size)
//            this[i] = generator.invoke(i)
//    }
//
//    constructor(rows: Int, cols: Int, data: DoubleArray) : this(rows, cols) {
//        check(size == data.size)
//        System.arraycopy(data, 0, array, 0, size)
//    }
//
//    constructor(rows: Int, cols: Int, exp: Exp<Double>) : this(rows, cols) {
//        check(size == exp.size)
//        for(i in 0 until size)
//            this[i] = exp[i]
//    }
//
//    constructor(exp: Exp<Double>) : this(exp.size) {
//        check(size == exp.size)
//        for(i in 0 until size)
//            this[i] = exp[i]
//    }
//
//    /* ========================================================================== */
//    /* Methods                                                                    */
//    /* ========================================================================== */
//
//    fun copy() = Matrix(rows, cols, array)
//
////    fun transposeVector() : Matrix {
////        check(isVector) { "Matrix transpose not supported!" }
////        val temp = rows
////        rows = cols
////        cols = temp
////        return this
////    }
//
//    companion object {
//        fun times(A: Matrix, B: Matrix): Matrix {
//            check(A.cols == B.rows) { "Dimension mismatch! [${A.rows}x${A.cols}] * [${B.rows}x${B.cols}]" }
//            val result = Matrix(A.rows, B.cols)
//            var di = 0
//            for (row in 0 until A.rows)
//                for (col in 0 until B.cols) {
//                    var sum = 0.0
//                    for (i in 0 until A.cols)
//                        sum += A[row, i] * B[i, col]
//                    result[di] = sum
//                    di += 1
//                }
//            return result
//        }
//
//        fun transposedTimes(A: Matrix, B: Matrix): Matrix {
//            check(A.rows == B.rows) { "Dimension mismatch! [${A.rows}x${A.cols}]' * [${B.rows}x${B.cols}]" }
//            val result = Matrix(A.cols, B.cols)
//            var di = 0
//            for (row in 0 until A.cols) {
//                for (col in 0 until B.cols) {
//                    var sum = 0.0
//                    for (i in 0 until B.cols)
//                        sum += A[i, row] * B[i, col]
//                    result[di] = sum
//                    di += 1
//                }
//            }
//            return result
//        }
//
//        fun timesTranspose(A: Matrix, B: Matrix): Matrix {
//            check(A.cols == B.cols) { "Dimension mismatch! [${A.rows}x${A.cols}] * [${B.rows}x${B.cols}]'" }
//            val result = Matrix(A.rows, B.rows)
//            var di = 0
//            for (row in 0 until A.rows) {
//                for (col in 0 until B.rows) {
//                    var sum = 0.0
//                    for (i in 0 until A.cols)
//                        sum += A[row, i] * B[col, i]
//                    result[di] = sum
//                    di += 1
//                }
//            }
//            return result
//        }
//    }
//
//    /* ========================================================================== */
//    /* Util                                                                       */
//    /* ========================================================================== */
//
////    companion object {
////        fun plus(A: Matrix, b: Double)      = A.copy().plus(b)
////        fun plus(A: Matrix, B: Matrix)      = A.copy().plus(B)
////        fun minus(A: Matrix, b: Double)     = A.copy().minus(b)
////        fun minus(A: Matrix, B: Matrix)     = A.copy().minus(B)
////        fun div(A: Matrix, b: Double)       = A.copy().div(b)
////        fun div(A: Matrix, B: Matrix)       = A.copy().div(B)
////        fun rem(A: Matrix, b: Double)       = A.copy().rem(b)
////        fun rem(A: Matrix, B: Matrix)       = A.copy().rem(B)
////        fun times(A: Matrix, b: Double)     = A.copy().times(b)
////        fun hadamard(A: Matrix, B: Matrix)  = A.copy().hadamard(B)
////    }
//
//    fun toSizeString(): String = "[${rows}x${cols}]"
//
//    fun toDataString(): String {
//        val sb = StringBuilder()
//        sb.append('[')
//        for(row in 0 until rows) {
//            if(row > 0)
//                sb.append("\n,")
//            sb.append('[')
//            for (col in 0 until cols) {
//                if(col > 0)
//                    sb.append(' ')
//                val offset = if(this[row, col] > 0) Math.ceil(Math.log10(this[row, col]) + 1).toInt() else 0
//                sb.append("%.${12-offset}f".format(this[row, col]))
//            }
//            sb.append(']')
//        }
//        sb.append("]\n")
//        return sb.toString()
//    }
//
//}

