package com.olhcim.common.math.expressions

/* ========================================================================== */
/* Interface                                                                  */
/* ========================================================================== */

interface IArray2D<T> : Exp2D<T>, IArray1D<T> {
    operator fun set(row: Int, col: Int, value: T)

    val isColVector get() = (cols == 1)
    val isRowVector get() = (rows == 1)
    val isVector get() = isColVector || isRowVector
}

/* ========================================================================== */
/* High Performance Assignment Template Fucntions                             */
/* ========================================================================== */

/*0*/ inline infix fun <T> IArray2D<T>.assign2d(
        operator: (index: Int, row: Int, col: Int) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col); it += 1; } }

/*1*/ inline fun <T> IArray2D<T>.assign2d(
        a: IArray2D<T>,
        operator: (index: Int, row: Int, col: Int, a: T) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col, a[it]); it += 1; } }
/*2*/ inline fun <T> IArray2D<T>.assign2d(
        a: IArray2D<T>, b: IArray2D<T>,
        operator: (index: Int, row: Int, col: Int, a: T, b: T) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col, a[it], b[it]); it += 1 } }
/*3*/ inline fun <T> IArray2D<T>.assign2d(
        a: IArray2D<T>, b: IArray2D<T>, c: IArray2D<T>,
        operator: (index: Int, row: Int, col: Int, a: T, b: T, c: T) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col, a[it], b[it], c[it]); it += 1 } }
/*4*/ inline fun <T> IArray2D<T>.assign2d(
        a: IArray2D<T>, b: IArray2D<T>, c: IArray2D<T>, d: IArray2D<T>,
        operator: (index: Int, row: Int, col: Int, a: T, b: T, c: T, d: T) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col, a[it], b[it], c[it], d[it]); it += 1 } }
/*5*/ inline fun <T> IArray2D<T>.assign2d(
        a: IArray2D<T>, b: IArray2D<T>, c: IArray2D<T>, d: IArray2D<T>, e: IArray2D<T>,
        operator: (index: Int, row: Int, col: Int, a: T, b: T, c: T, d: T, e: T) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col, a[it], b[it], c[it], d[it], e[it]); it += 1 } }
/*6*/ inline fun <T> IArray2D<T>.assign2d(
        a: IArray2D<T>, b: IArray2D<T>, c: IArray2D<T>, d: IArray2D<T>, e: IArray2D<T>, f: IArray2D<T>,
        operator: (index: Int, row: Int, col: Int, a: T, b: T, c: T, d: T, e: T, f: T) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col, a[it], b[it], c[it], d[it], e[it], f[it]); it += 1 } }
/*7*/ inline fun <T> IArray2D<T>.assign2d(
        a: IArray2D<T>, b: IArray2D<T>, c: IArray2D<T>, d: IArray2D<T>, e: IArray2D<T>, f: IArray2D<T>, g: IArray2D<T>,
        operator: (index: Int, row: Int, col: Int, a: T, b: T, c: T, d: T, e: T, f: T, g: T) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col, a[it], b[it], c[it], d[it], e[it], f[it], g[it]); it += 1 } }
/*8*/ inline fun <T> IArray2D<T>.assign2d(
        a: IArray2D<T>, b: IArray2D<T>, c: IArray2D<T>, d: IArray2D<T>, e: IArray2D<T>, f: IArray2D<T>, g: IArray2D<T>, h: IArray2D<T>,
        operator: (index: Int, row: Int, col: Int, a: T, b: T, c: T, d: T, e: T, f: T, g: T, h: T) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col, a[it], b[it], c[it], d[it], e[it], f[it], g[it], h[it]); it += 1 } }
/*9*/ inline fun <T> IArray2D<T>.assign2d(
        a: IArray2D<T>, b: IArray2D<T>, c: IArray2D<T>, d: IArray2D<T>, e: IArray2D<T>, f: IArray2D<T>, g: IArray2D<T>, h: IArray2D<T>, i: IArray2D<T>,
        operator: (index: Int, row: Int, col: Int, a: T, b: T, c: T, d: T, e: T, f: T, g: T, h: T, i: T) -> T
) { var it = 0; for(row in 0 until rows) for(col in 0 until cols) { this[it] = operator(it, row, col, a[it], b[it], c[it], d[it], e[it], f[it], g[it], h[it], i[it]); it += 1 } }



/* ========================================================================== */
/* Array Implementations                                                      */
/* ========================================================================== */

open class Array2DConst<T>(final override val rows: Int, final override val cols: Int, const: T) : IArray2D<T>, Array1DConst<T>(rows*cols, const) {
    override val size = rows*cols
    override fun get(row: Int, col: Int) = const
    override fun set(row: Int, col: Int, value: T) = throw UnsupportedOperationException("Cannot set const array2D!")
}

abstract class AbstractArray2D<T>(
        final override val rows: Int,
        final override val cols: Int
) : IArray2D<T> {
    final override fun get(row: Int, col: Int) = this[rowColToIndex(row, col)]
    final override fun set(row: Int, col: Int, value: T) { this[rowColToIndex(row, col)] = value }
    override fun toString() = "[${rows}x${cols}]"
}

open class Array2DDouble(
        rows: Int,
        cols: Int,
        val array: DoubleArray
) : AbstractArray2D<Double>(rows, cols) {
    init { check(size == array.size) }

    override fun get(i: Int) = array[i]
    override fun set(i: Int, value: Double) { array[i] = value }

    constructor(rows: Int, cols: Int, generator: ((i: Int) -> Double)? = null) : this(rows, cols, DoubleArray(rows*cols)) {
        if(generator != null) for(i in 0 until size) array[i] = generator(i)
    }

    constructor(data: DoubleArray) : this(data.size, 1, data)

    constructor(rows: Int, generator: ((i: Int) -> Double)? = null) : this(rows, 1, generator)

    constructor(exp: Exp2D<Double>) : this(exp.rows, exp.cols) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    constructor(exp: Exp<Double>) : this(exp.size) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    override fun copy(): Array2DDouble {
        val arr = DoubleArray(size)
        System.arraycopy(array, 0, arr, 0, size)
        return Array2DDouble(rows, cols, arr)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append('[')
        for(row in 0 until rows) {
            if(row > 0)
                sb.append("\n,")
            sb.append('[')
            for (col in 0 until cols) {
                if(col > 0)
                    sb.append(' ')
                val offset = if(this[row, col] > 0) Math.ceil(Math.log10(this[row, col]) + 1).toInt() else 0
                sb.append("%.${12-offset}f".format(this[row, col]))
            }
            sb.append(']')
        }
        sb.append("]")
        return sb.toString()
    }
}

open class Array2DInt(
        rows: Int,
        cols: Int,
        val array: IntArray
) : AbstractArray2D<Int>(rows, cols) {
    init { check(size == array.size) }

    override fun get(i: Int) = array[i]
    override fun set(i: Int, value: Int) { array[i] = value }

    constructor(rows: Int, cols: Int, generator: ((i: Int) -> Int)? = null) : this(rows, cols, IntArray(rows*cols)) {
        if(generator != null) for(i in 0 until size) array[i] = generator(i)
    }

    constructor(data: IntArray) : this(data.size, 1, data)

    constructor(rows: Int, generator: ((i: Int) -> Int)? = null) : this(rows, 1, generator)

    constructor(exp: Exp2D<Int>) : this(exp.rows, exp.cols) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    constructor(exp: Exp<Int>) : this(exp.size) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    override fun copy(): Array2DInt {
        val arr = IntArray(size)
        System.arraycopy(array, 0, arr, 0, size)
        return Array2DInt(rows, cols, arr)
    }
}

open class Array2DBoolean(
        rows: Int,
        cols: Int,
        val array: BooleanArray
) : AbstractArray2D<Boolean>(rows, cols) {
    init { check(size == array.size) }

    override fun get(i: Int) = array[i]
    override fun set(i: Int, value: Boolean) { array[i] = value }

    constructor(rows: Int, cols: Int, generator: ((i: Int) -> Boolean)? = null) : this(rows, cols, BooleanArray(rows*cols)) {
        if(generator != null) for(i in 0 until size) array[i] = generator(i)
    }

    constructor(data: BooleanArray) : this(data.size, 1, data)

    constructor(rows: Int, generator: ((i: Int) -> Boolean)? = null) : this(rows, 1, generator)

    constructor(exp: Exp2D<Boolean>) : this(exp.rows, exp.cols) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    constructor(exp: Exp<Boolean>) : this(exp.size) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    override fun copy(): Array2DBoolean {
        val arr = BooleanArray(size)
        System.arraycopy(array, 0, arr, 0, size)
        return Array2DBoolean(rows, cols, arr)
    }
}


open class Array2D<T>(
        rows: Int,
        cols: Int,
        val array: Array<Any?>
) : AbstractArray2D<T?>(rows, cols) {

    override fun get(i: Int) = array[i] as T?
    override fun set(i: Int, value: T?) { array[i] = value }

    constructor(rows: Int, cols: Int, generator: ((i: Int) -> T?)? = null) : this(rows, cols, Array<Any?>(rows*cols) { null }) {
        if(generator != null) for(i in 0 until size) array[i] = generator(i)
    }

    constructor(data: Array<T?>) : this(data.size, 1, data as Array<Any?>)

    constructor(rows: Int, generator: ((i: Int) -> T)? = null) : this(rows, 1, generator)

    constructor(exp: Exp2D<T>) : this(exp.rows, exp.cols) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    constructor(exp: Exp<T>) : this(exp.size) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    override fun copy(): Array2D<T> {
        val arr = Array<Any?>(rows*cols) { null }
        System.arraycopy(array, 0, arr, 0, size)
        return Array2D<T>(rows, cols, arr)
    }
}




