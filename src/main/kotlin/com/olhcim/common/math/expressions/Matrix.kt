package com.olhcim.common.math.expressions

open class Matrix(
        rows: Int,
        cols: Int,
        array: DoubleArray
) : Array2DDouble(rows, cols, array) {
    constructor(rows: Int, cols: Int, generator: ((i: Int) -> Double)? = null) : this(rows, cols, DoubleArray(rows*cols)) {
        if(generator != null) for(i in 0 until size) array[i] = generator(i)
    }

    constructor(data: DoubleArray) : this(data.size, 1, data)

    constructor(rows: Int, generator: ((i: Int) -> Double)? = null) : this(rows, 1, generator)

    constructor(exp: Exp2D<Double>) : this(exp.rows, exp.cols) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    constructor(exp: Exp<Double>) : this(exp.size) {
        check(size == exp.size); for(i in 0 until size) array[i] = exp[i]
    }

    override fun copy(): Matrix {
        val arr = DoubleArray(size)
        System.arraycopy(array, 0, arr, 0, size)
        return Matrix(rows, cols, arr)
    }
}

/* ========================================================================== */
/* Basic Operators                                                            */
/* ========================================================================== */

fun matmul(A: Matrix, B: Matrix, output: Matrix? = null): Matrix {
    check(A.cols == B.rows) { "Dimension mismatch! [${A.rows}x${A.cols}] * [${B.rows}x${B.cols}]" }
    val result = output ?: Matrix(A.rows, B.cols)
    var di = 0
    for (row in 0 until A.rows)
        for (col in 0 until B.cols) {
            var sum = 0.0
            for (i in 0 until A.cols)
                sum += A[row, i] * B[i, col]
            result[di] = sum
            di += 1
        }
    return result
}

fun transposedMatmul(A: Matrix, B: Matrix, output: Matrix? = null): Matrix {
    check(A.rows == B.rows) { "Dimension mismatch! [${A.cols}x${A.rows}]' * [${B.rows}x${B.cols}]" }
    val result = output ?: Matrix(A.cols, B.cols)
    var di = 0
    for (row in 0 until A.cols) {
        for (col in 0 until B.cols) {
            var sum = 0.0
            for (i in 0 until B.cols)
                sum += A[i, row] * B[i, col]
            result[di] = sum
            di += 1
        }
    }
    return result
}

fun matmulTranspose(A: Matrix, B: Matrix, output: Matrix? = null): Matrix {
    check(A.cols == B.cols) { "Dimension mismatch! [${A.rows}x${A.cols}] * [${B.cols}x${B.rows}]'" }
    val result = output ?: Matrix(A.rows, B.rows)
    var di = 0
    for (row in 0 until A.rows) {
        for (col in 0 until B.rows) {
            var sum = 0.0
            for (i in 0 until A.cols)
                sum += A[row, i] * B[col, i]
            result[di] = sum
            di += 1
        }
    }
    return result
}