package com.olhcim.common.math.expressions

/* ========================================================================== */
/* Interface                                                                  */
/* ========================================================================== */

interface IIndices2D {
    val cols: Int
    fun rowColToIndex(row: Int, col: Int): Int = (row* cols)+col
    fun indexToCol(i: Int): Int = i % cols
    fun indexToRow(i: Int): Int = i / cols
}

interface Exp2D<T> : Exp<T>, IIndices2D {
    val rows: Int
    override val cols: Int
    override val size: Int get() = rows*cols
    operator fun get(row: Int, col: Int): T
}



/* ========================================================================== */
/* Basic Expression Operators - Left for rapid prototyping... Originally used */
/* classes, this is 2x faster but still slow, use hard coded inline functions */
/* ========================================================================== */

/* Inefficient with Exp implementations already existing */

//inline operator fun Exp2D<Double>.plus (b: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] + b[r, c] }
//inline operator fun Exp2D<Double>.minus(b: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] + b[r, c] }
//inline operator fun Exp2D<Double>.times(b: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] + b[r, c] }
//inline operator fun Exp2D<Double>.div  (b: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] + b[r, c] }
//inline operator fun Exp2D<Double>.rem  (b: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] + b[r, c] }

//inline operator fun ((r: Int, c: Int) -> Double).plus (arr: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) + arr[r, c] }
//inline operator fun ((r: Int, c: Int) -> Double).minus(arr: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) + arr[r, c] }
//inline operator fun ((r: Int, c: Int) -> Double).times(arr: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) + arr[r, c] }
//inline operator fun ((r: Int, c: Int) -> Double).div  (arr: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) + arr[r, c] }
//inline operator fun ((r: Int, c: Int) -> Double).rem  (arr: Exp2D<Double>) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) + arr[r, c] }

//inline operator fun ((r: Int, c: Int) -> Double).plus (crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) + operator(it) }
//inline operator fun ((r: Int, c: Int) -> Double).minus(crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) - operator(it) }
//inline operator fun ((r: Int, c: Int) -> Double).times(crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) * operator(it) }
//inline operator fun ((r: Int, c: Int) -> Double).div  (crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) / operator(it) }
//inline operator fun ((r: Int, c: Int) -> Double).rem  (crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this(r, c) % operator(it) }

//inline operator fun Exp2D<Double>.plus (crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] + operator(r, c) }
//inline operator fun Exp2D<Double>.minus(crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] - operator(r, c) }
//inline operator fun Exp2D<Double>.times(crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] * operator(r, c) }
//inline operator fun Exp2D<Double>.div  (crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] / operator(r, c) }
//inline operator fun Exp2D<Double>.rem  (crossinline operator: (r: Int, c: Int) -> Double) : ((r: Int, c: Int) -> Double) = { r, c -> this[r, c] % operator(r, c) }

// OtherOperators