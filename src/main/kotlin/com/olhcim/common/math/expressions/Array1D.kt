package com.olhcim.common.math.expressions



/* ========================================================================== */
/* Interface                                                                  */
/* ========================================================================== */

interface IArray1D<T> : Exp<T> {
    operator fun set(i: Int, value: T)
    fun copy(): IArray1D<T>
}



/* ========================================================================== */
/* Basic Operators                                                            */
/* ========================================================================== */

inline infix fun <T> IArray1D<T>.assign(operator: (i: Int) -> T) { for(i in 0 until size) this[i] = operator(i) }
//inline infix fun <T> IArray1D<T>.assign(operator: (i: Int, value: T) -> T) { for(i in 0 until size) this[i] = operator(i, this[i]) }

operator fun IArray1D<Double>.plusAssign (operator: (i: Int) -> Double) { for(i in 0 until size) this[i] += operator(i) }
operator fun IArray1D<Double>.minusAssign(operator: (i: Int) -> Double) { for(i in 0 until size) this[i] -= operator(i) }
operator fun IArray1D<Double>.timesAssign(operator: (i: Int) -> Double) { for(i in 0 until size) this[i] *= operator(i) }
operator fun IArray1D<Double>.divAssign  (operator: (i: Int) -> Double) { for(i in 0 until size) this[i] /= operator(i) }
operator fun IArray1D<Double>.remAssign  (operator: (i: Int) -> Double) { for(i in 0 until size) this[i] %= operator(i) }

//inline infix fun <T> IArray1D<T>.assign(const: T) { for(i in 0 until size) this[i] = const }

operator fun IArray1D<Double>.plusAssign (const: Double) { for(i in 0 until size) this[i] += const }
operator fun IArray1D<Double>.minusAssign(const: Double) { for(i in 0 until size) this[i] -= const }
operator fun IArray1D<Double>.timesAssign(const: Double) { for(i in 0 until size) this[i] *= const }
operator fun IArray1D<Double>.divAssign  (const: Double) { for(i in 0 until size) this[i] /= const }
operator fun IArray1D<Double>.remAssign  (const: Double) { for(i in 0 until size) this[i] %= const }


inline infix fun <T> IArray1D<T>.assign(exp: Exp<T>) { for(i in 0 until size) this[i] = exp[i] }

operator fun IArray1D<Double>.plusAssign (exp: Exp<Double>) { for(i in 0 until size) this[i] += exp[i] }
operator fun IArray1D<Double>.minusAssign(exp: Exp<Double>) { for(i in 0 until size) this[i] -= exp[i] }
operator fun IArray1D<Double>.timesAssign(exp: Exp<Double>) { for(i in 0 until size) this[i] *= exp[i] }
operator fun IArray1D<Double>.divAssign  (exp: Exp<Double>) { for(i in 0 until size) this[i] /= exp[i] }
operator fun IArray1D<Double>.remAssign  (exp: Exp<Double>) { for(i in 0 until size) this[i] %= exp[i] }



/* ========================================================================== */
/* High Performance Assignment Template Fucntions                             */
/* ========================================================================== */

/*1*/ inline fun <T> IArray1D<T>.assign(
        a: Exp<T>,
        operator: (a: T) -> T
) { for(it in 0 until a.size) this[it] = operator(a[it]) }
/*2*/ inline fun <T> IArray1D<T>.assign(
        a: Exp<T>, b: Exp<T>,
        operator: (a: T, b: T) -> T
) { for(it in 0 until a.size) this[it] = operator(a[it], b[it]) }
/*3*/ inline fun <T> IArray1D<T>.assign(
        a: Exp<T>, b: Exp<T>, c: Exp<T>,
        operator: (a: T, b: T, c: T) -> T
) { for(it in 0 until a.size) this[it] = operator(a[it], b[it], c[it]) }
/*4*/ inline fun <T> IArray1D<T>.assign(
        a: Exp<T>, b: Exp<T>, c: Exp<T>, d: Exp<T>,
        operator: (a: T, b: T, c: T, d: T) -> T
) { for(it in 0 until a.size) this[it] = operator(a[it], b[it], c[it], d[it]) }
/*5*/ inline fun <T> IArray1D<T>.assign(
        a: Exp<T>, b: Exp<T>, c: Exp<T>, d: Exp<T>, e: Exp<T>,
        operator: (a: T, b: T, c: T, d: T, e: T) -> T
) { for(it in 0 until a.size) this[it] = operator(a[it], b[it], c[it], d[it], e[it]) }
/*6*/ inline fun <T> IArray1D<T>.assign(
        a: Exp<T>, b: Exp<T>, c: Exp<T>, d: Exp<T>, e: Exp<T>, f: Exp<T>,
        operator: (a: T, b: T, c: T, d: T, e: T, f: T) -> T
) { for(it in 0 until a.size) this[it] = operator(a[it], b[it], c[it], d[it], e[it], f[it]) }
/*7*/ inline fun <T> IArray1D<T>.assign(
        a: Exp<T>, b: Exp<T>, c: Exp<T>, d: Exp<T>, e: Exp<T>, f: Exp<T>, g: Exp<T>,
        operator: (a: T, b: T, c: T, d: T, e: T, f: T, g: T) -> T
) { for(it in 0 until a.size) this[it] = operator(a[it], b[it], c[it], d[it], e[it], f[it], g[it]) }
/*8*/ inline fun <T> IArray1D<T>.assign(
        a: Exp<T>, b: Exp<T>, c: Exp<T>, d: Exp<T>, e: Exp<T>, f: Exp<T>, g: Exp<T>, h: Exp<T>,
        operator: (a: T, b: T, c: T, d: T, e: T, f: T, g: T, h: T) -> T
) { for(it in 0 until a.size) this[it] = operator(a[it], b[it], c[it], d[it], e[it], f[it], g[it], h[it]) }
/*9*/ inline fun <T> IArray1D<T>.assign(
        a: Exp<T>, b: Exp<T>, c: Exp<T>, d: Exp<T>, e: Exp<T>, f: Exp<T>, g: Exp<T>, h: Exp<T>, i: Exp<T>,
        operator: (a: T, b: T, c: T, d: T, e: T, f: T, g: T, h: T, i: T) -> T
) { for(it in 0 until a.size) this[it] = operator(a[it], b[it], c[it], d[it], e[it], f[it], g[it], h[it], i[it]) }



/* ========================================================================== */
/* Array Implementations                                                      */
/* ========================================================================== */

open class Array1DConst<T>(override val size: Int, val const: T) : IArray1D<T> {
    override fun get(i: Int) = const
    override fun set(i: Int, value: T) = throw UnsupportedOperationException("Cannot set const array!")
    override fun copy() = Array1DConst(size, const)
}

//abstract class AbstractArray1D<T>(override val size: Int) : IArray1D<T> {
//    override fun toString(): String {
//        val sb = StringBuilder()
//        sb.append('[')
//        for (col in 0 until size) {
//            if(col > 0)
//                sb.append(' ')
//            val offset = if(this[col] > 0) Math.ceil(Math.log10(this[col]) + 1).toInt() else 0
//            sb.append("%.${12-offset}f".format(this[col]))
//        }
//        sb.append("]")
//        return sb.toString()
//    }
//}


open class Array1DDouble(val array: DoubleArray) : IArray1D<Double> {
    override val size = array.size
    constructor(size: Int, generator: ((i: Int) -> Double)? = null) : this(DoubleArray(size)) {
        if(generator != null) for(i in 0 until size) array[i] = generator.invoke(i)
    }
    override fun get(i: Int) = array[i]
    override fun set(i: Int, value: Double) { array[i] = value }
    override fun copy(): Array1DDouble {
        val arr = DoubleArray(size)
        System.arraycopy(array, 0, arr, 0, size)
        return Array1DDouble(arr)
    }
}

open class Array1DInt(val array: IntArray) : IArray1D<Int> {
    override val size = array.size
    constructor(size: Int, generator: ((i: Int) -> Int)? = null) : this(IntArray(size)) {
        if(generator != null) for(i in 0 until size) array[i] = generator.invoke(i)
    }
    override fun get(i: Int) = array[i]
    override fun set(i: Int, value: Int) { array[i] = value }
    override fun copy(): Array1DInt {
        val arr = IntArray(size)
        System.arraycopy(array, 0, arr, 0, size)
        return Array1DInt(arr)
    }
}










