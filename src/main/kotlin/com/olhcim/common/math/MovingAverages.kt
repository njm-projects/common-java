package com.olhcim.common.math

import com.olhcim.common.collection.circular.FixedCircularArray

class ExponentialMovingAverage(val eta: Double = 0.02) {

    private var first: Boolean = true
    private var ave: Double = 0.0

    fun update(value: Double): Double {
        if(first) {
            ave = value
            first = false
        } else {
            ave = (1 - eta) * ave + eta * value
        }
        return ave
    }

    fun reset() {
        first = true
        ave = 0.0
    }
}


class NMovingAverage(val n: Int) {
    private val queue = FixedCircularArray<Double>(n)
    private var total: Double = 0.0

    fun update(value: Double): Double {
        if (queue.size >= n)
            total -= queue.popFront()
        total += value
        queue.pushBack(value)
        return total / queue.size
    }

    fun reset() {
        queue.clear()
        total = 0.0
    }
}


class MovingAverage {
    private var first: Boolean = true
    private var count: Long = 0L
    private var ave: Double = 0.0

    fun update(value: Double): Double {
        ave = (count*ave + value)
        count += 1
        ave /= count
        return ave
    }

    fun reset() {
        first = true
        count = 0L
        ave = 0.0
    }
}