package com.olhcim.common.math

import java.security.SecureRandom
import java.util.Random

/**
 * Created by Nathan on 2016/09/10.
 */
class FixedRandom {

    private var random: Random

    constructor() {
        this.random = SecureRandom()
    }

    constructor(seed: Long) {
        this.random = Random(seed)
    }

    fun nextInt(): Int
            = random.nextInt()

    fun nextInt(bound: Int): Int
            = random.nextInt(bound)

    fun nextDouble(): Double
            = random.nextDouble()

    fun nextFloat(): Float
            = random.nextFloat()

    fun nextLong(): Long
            = random.nextLong()

    fun nextBoolean(): Boolean
            = random.nextBoolean()

    fun nextGaussian(): Double
            = random.nextGaussian()
}
