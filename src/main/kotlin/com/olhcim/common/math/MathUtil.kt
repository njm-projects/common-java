package com.olhcim.common.math

object Distance {

    fun manhatan(x0: Int, y0: Int, x1: Int, y1: Int): Int {
        return Math.abs(x0 - x1) + Math.abs(y0 - y1)
    }

    fun euclidean(x0: Int, y0: Int, x1: Int, y1: Int): Int {
        return Math.sqrt(((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1)).toDouble()).toInt()
    }

    fun minkovski(x0: Int, y0: Int, x1: Int, y1: Int, p: Double): Int {
        return Math.pow(Math.pow(Math.abs(x0 - x1).toDouble(), p) + Math.pow(Math.abs(y0 - y1).toDouble(), p), 1 / p).toInt()
    }

}

object Normalise {

    fun range(min: Double, max: Double, newmin: Double, newmax: Double, x: Double): Double {
        return (x - min) * (newmax - newmin) / (max - min) + newmin
    }

    fun range(min: Int, max: Int, newmin: Int, newmax: Int, x: Int): Int {
        return (x - min) * (newmax - newmin) / (max - min) + newmin
    }

}

object Interpolate {

    fun linear(a: Double, b: Double, ratio: Double): Double {
        return a + (b - a) * ratio
    }

    fun linear(a: Int, b: Int, ratio: Double): Int {
        return (a + (b - a) * ratio).toInt()
    }

    fun linear(a: Int, b: Int, num: Int, pos: Int): Int {
        return a + (b - a) * pos / num.coerceAtLeast(1)
    }
}